<?PHP

error_reporting ( E_ALL ) ;

include_once ( "common.php" ) ;

$language = fix_language_code ( get_request ( 'language' , 'en' ) , 'en' ) ;
$project = check_project_name ( get_request ( 'project' , 'wikipedia' ) ) ;
$template = get_request ( 'template' , 'Template:GeoTemplate/Group' ) ;
$pagetitle = get_request ( 'pagetitle' , '' ) ;
$nicepagetitle = str_replace ( '_' , ' ' , $pagetitle ) ;

$main_title = "Geographic groups for \"$nicepagetitle\"" ;

print "<html>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' ;
print '<style type="text/css">
.editsection { display : none }
</style>' ;
print "<title>$main_title</title>" ;
print "</head><body>" ;
print get_common_header ( "kmlhook.php" ) . "\n" ;

$text = file_get_contents ( "http://$language.$project.org//w/index.php?action=render&title=$template" ) ;

$text = str_ireplace ( '$$NICEPAGETITLE$$' , $nicepagetitle , $text ) ;
$text = str_ireplace ( '$$PAGETITLE$$' , urlencode ( $pagetitle ) , $text ) ;
$text = str_ireplace ( '$$LANGUAGE$$' , $language , $text ) ;
$text = str_ireplace ( '$$PROJECT$$' , $project , $text ) ;

print $text ;



print "</body>" ;
print "</html>\n" ;
myflush() ;

?>
