<?php

error_reporting ( E_ALL ) ;
@set_time_limit ( 20*60 ) ; # Time limit 20min
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

include_once ( 'queryclass.php' ) ;


function get_edit_timestamp ( $lang , $project , $title ) {
  $t = "http://{$lang}.{$project}.org/w/index.php?title=Special:Export/" . myurlencode ( $title ) ;
  $t = @file_get_contents ( $t ) ;
# $desc = $t ;
  $t = explode ( '<timestamp>' , $t , 2 ) ;
  $t = explode ( '</timestamp>' , array_pop ( $t ) , 2 ) ;
  $t = array_shift ( $t ) ;
  $t = str_replace ( '-' , '' , $t ) ;
  $t = str_replace ( ':' , '' , $t ) ;
  $t = str_replace ( 'T' , '' , $t ) ;
  $t = str_replace ( 'Z' , '' , $t ) ;
  return $t ;
}

print '<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script>
function trim(s) {
  while (s.substring(0,1) == "\n") {
    s = s.substring(1,s.length);
  }
  while (s.substring(s.length-1,s.length) == "\n") {
    s = s.substring(0,s.length-1);
  }
  return s;
}
function add_to_textarea ( ta_id , text , add ) {
  ta = document.getElementById ( ta_id ) ;
  t = ta.value 
  t = "\n" + t + "\n" ;
  t = t.split ( "\n" + text + "\n" ) . join ( "\n" ) ;
  t = trim ( t ) ;
  if ( add ) t = trim ( t + "\n" + text ) ;
  ta.value =  t ;
}
function remove_uncat ( ta_id ) {
  ta = document.getElementById ( ta_id ) ;  
  t = ta.value ;
  arr = t.toLowerCase().split("[[category:") ;
  if ( arr.length < 2 ) return ;
  arr = t.split ( "\n" ) ;
  t = "" ;
  for ( i = 0 ; i < arr.length ; i++ ) {
  	l = arr[i].toLowerCase() ;
  	l = l.substr ( 0 , 9 ) ;
  	if ( l == "{{uncat}}" ) continue ;
  	t += arr[i] + "\n" ;
  }
  ta.value = t ;
}
</script>
<title>CatLitter</title></head><body>' ;

print get_common_header ( 'catlitter.php' ) ;

$is_on_toolserver = false ;
$nameofthetool = 'catlitter' ;
$category = 'Media needing categories' ;
$language = 'commons' ;
$project = 'wikimedia' ;
$maxw = 120 ;
$subset = 20 ;
$templates = array ( 'Bad source' , 'Cleanup image' , 'Delete' , 'Description missing' , 'No license' , 'No source' , 'Speedydelete' ) ;

print "<h1>CatLitter</h1>" ;
print "A tool to mass-add categories and templates to images. Images in \"Category:$category\" are grouped by user, as users tend to upload similar files.<br/>" ;
print "Analyzing \"Category:$category\". This might take a few seconds...<br/>" ;
myflush();

$users = db_get_users_in_cat ( $category , $language , $project ) ;
print count ( $users ) . " distinct users have uploaded files to \"Category:$category\".<br/>" ;
print "Selecting $subset users at random...<br/>" ;

$user_ids = array_rand ( $users , $subset ) ; # KEYS!!!

$images = db_get_images_by_users_in_cat ( $category , $user_ids , $language , $project ) ;
$wq = new WikiQuery ( $language , $project ) ;

print "Using " . count ( $images ) . " images from category \"$category\"<br/>" ;
myflush() ;

print "<table border='1'>" ;

$cnt = 0 ;
foreach ( $images AS $ia ) {
  $i = "Image:" . $ia->img_name ;
  $i2 = array_pop ( explode ( ':' , $i , 2 ) ) ;
  $i2 = str_replace ( '_' , ' ' , $i2 ) ;
  $size = $ia->img_size ;
  $width = $ia->img_width ;
  $height = $ia->img_height ;
  if ( $width > $height ) $nw = $maxw ;
  else if ( $height == 0 ) $nw = 0 ;
  else if ( $width <= $maxw && $height <= $maxw ) $nw = $width ;
  else $nw = round ( $width * $maxw / $height ) ;
  $thumb_url = get_thumbnail_url ( $language , $i2 , $nw , $project ) ;
  $image_url = get_wikipedia_url ( $language , $i , '' , $project ) ;
  
  $text = get_wikipedia_article ( $language , $i , false , $project ) ;
  $lines = explode ( "\n" , $text ) ;
  $text = "" ;
  foreach ( $lines AS $l ) {
    $l2 = strtolower ( trim ( $l ) ) ;
//    if ( substr ( $l2 , 0 , 9 ) == '{{uncat}}' ) continue ;
    $text .= "$l\n" ;
  }
  
  $ts = get_edit_timestamp ( $language , $project , $i ) ;
  $te = $ts ;
  
  $cs = common_sense ( $language , $i ) ;
  if ( count ( $cs ) < 1 ) $cs = array () ;
  $cl = count ( $cs ) + 11 ;
  
  print "<tr>" ;

  print "<form style='display:inline' method='post' action=\"http://commons.wikimedia.org/w/index.php?title=" ;
  print myurlencode ( $i ) ;
  print "&action=edit\" target='_blank'>" ;

  print "<td align='center' valign='top'>" ;
  print "<a href=\"$image_url\" target='_blank'>" ;
  if ( $nw > 0 ) print "<img src=\"$thumb_url\" border=0 /><br/>" ;
  print "$i2</a><br/>" ;
  print "$size bytes, " ;
  print "$width&times;$height<br>" ;
  print "Uploaded by {$ia->img_user_text}" ;
  print "</td>" ;
  
  print "<td valign='top' nowrap>" ;
  print "Categories:<br/>" ;
  foreach ( $cs AS $k => $v ) {
    if ( $v == 'Categories' ) continue ; # No need ;-)
#    $v = utf8_decode ( $v ) ;
    $id = "nc_" . $cnt . "_" . $k ;
    print "<input type='checkbox' name='newcats_$cnt' id='$id' value=\"$v\" onclick=\"add_to_textarea('ta_$cnt','[[Category:$v]]',this.checked);\"/>" ;
    print "<label for='$id'>$v</label><br/>" ;
  }
  print "<hr/>Templates:<br/>" ;
  foreach ( $templates AS $v ) {
    $id = "nt_" . $cnt . "_" . $v ;
    print "<input type='checkbox' id='$id' value=\"$v\" onclick=\"add_to_textarea('ta_$cnt','\{\{$v\}\}',this.checked);\"/>" ;
    print "<label for='$id'>$v</label><br/>" ;
  }
  print "</td>" ;
  
  print "<td valign='top'>" ;
  print "<input type='hidden' name='wpStarttime' value='$ts'/>" ;
  print "<input type='hidden' name='wpEdittime' value='$te'/>" ;
  print "<input type='hidden' name='wpSummary' value='Added templates and/or categories with CatLitter'/>" ;
  print "<input type='hidden' name='wpMinoredit' value='1'/>" ;
  print "<textarea id='ta_$cnt' name='wpTextbox1' rows='$cl' cols='80'>" ;
  print $text ;
  print "</textarea><br/>" ;
  print "<input type='submit' name='wpDiff' value='Do it' onclick='remove_uncat(\"ta_$cnt\");return true;'/>" ;
  print ' ({{uncat}} will be removed if categories are present)' ;
  print "</td>" ;
  
  print "</form>" ;
  print "</tr>" ;
  myflush() ;
  $cnt++ ;
}


print "</table>" ;

?>