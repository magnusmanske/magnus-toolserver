<?PHP

include "common.php" ;
include_once ( "common_images.php" ) ;
include_once ( 'php/legacy.php' ) ;

function get_image_type ( $language , $title , $templates ) {
	global $good_templates , $evil_templates , $evil_templates_commons ;
	
	$status = 'unknown' ;
	foreach ( $templates AS $t ) {
		$t = strtolower ( $t ) ;
		$t2 = str_replace ( '_' , ' ' , $t ) ;
		if ( $language == 'commons' ) {
			if ( in_array ( $t , $evil_templates_commons ) ) return 'bad' ;
			if ( in_array ( $t2 , $evil_templates_commons ) ) return 'bad' ;
			continue ;
		}
		if ( in_array ( $t , $evil_templates ) ) return 'bad' ;
		if ( in_array ( $t2 , $evil_templates ) ) return 'bad' ;
		if ( in_array ( $t , $good_templates ) ) $status = 'good' ;
		if ( in_array ( $t2 , $good_templates ) ) $status = 'good' ;
	}
	if ( $language == "commons" ) return "commons" ;
	
	return $status ;
}

function show_main_form () {
	global $project , $language , $category , $depth , $cs2_url , $files , $user_files ;
	global $tusc_user , $tusc_password ;
	print "<table>" ;
	print "<tr><th>Language</th><td><input type='text' id='language' name='language' value='$language' /></td>" ;
	print "<th>Project</th><td><input type='text' name='project' value='$project' /></td></tr>" ;
	print "<tr><th>Category</th><td colspan='3'><input type='text' name='category' value='$category' size='80' /></td>" ;
	print "<th>Depth</th><td><input type='text' name='depth' value='$depth' />, <i>or</i></td></tr>" ;
	print "<tr><th>Files by user</th><td colspan='5'><input size='115' name='user_files' value='$user_files' />, <i>or</i></td></tr>" ;
	print "<tr><th>CatScan2 link</th><td colspan='5'><input size='115' name='cs2_url' value='$cs2_url' />, <i>or</i></td></tr>" ;
	print "<tr><th>List of files</th><td colspan='5'><textarea width='100%' cols='80' rows='5' name='files'>$files</textarea></td></tr>" ;
	print "<tr><th>TUSC user</th><td><input type='text' name='tusc_user' id='tusc_user' value='$tusc_user' /></td>" ;
	print "<th>Password</th><td><input type='password' name='tusc_password' id='tusc_password' value='$tusc_password' /></td><td colspan='2'><i>(Your <a href='http://toolserver.org/~magnus/tusc.php'>TUSC</a> login for Commons!)</i></td></tr>" ;
	print "</table>" ;
	print "<input type='submit' name='doit_look' value='Look for files to transfer' />" ;
}

function load_images_from_user () {
	global $user , $language , $project , $images , $limit ;
	$images = db_get_user_images ( $user , $language , $project ) ;
	while ( count ( $images ) > $limit ) array_pop ( $images ) ;
}

function get_thumb_link ( $image ) {
	global $language , $project ;
	$width = 120 ;
	$turl = get_thumbnail_url ( $language , $image , $width , $project ) ;
	$durl = get_wikipedia_url ( $language , "File:".$image , "" , $project ) ;
	$w = '' ;
	$end = strtolower ( substr ( $image , -4 , 4 ) ) ;
	if ( $end == '.gif' || $end == '.ogg' || $end == '.ogm' || $end == '.ogv' ) {
		$w = " width='{$width}px' " ;
	}
	return "<a target='_blank' href='$durl'><img border='0' $w src='$turl' /></a>" ;
}

function show_images () {
	global $language , $project , $images ;
	ksort ( $images ) ;
	print "<h2>Results</h2>" ;
	print "<table border='1'>" ;
	print "<tr><th>Transfer</th><th>Thumbnail</th><th>Image</th></tr>" ;
	$cnt = 0 ;
	foreach ( $images AS $i ) {
		$cnt++ ;
		$k = $cnt ;
		$i->img_name = trim ( str_replace ( "\r" , "" , $i->img_name ) ) ;
		$title = $i->img_name ;
		$title = str_replace ( '_' , ' ' , $title ) ;
		$templates = db_get_used_templates ( $language , $title , 6 , $project , 1 ) ;
		$status = get_image_type ( $language , $title , $templates ) ;
		$exists =  db_image_exists_size_sha1 ( $i->img_size , $i->img_sha1 , 'commons' , 'wikimedia' ) ;
		if ( $exists !== false ) $status = 'commons' ;
		$check = $status == 'good' ? 'checked' : '' ;
		
		print "<tr><td>" ;
		if ( false === $exists ) print "<input class='cbtransfer' type='checkbox' id='cb_$k' $check />" ;
		print "</td><td>" . get_thumb_link ( $title ) . "</td>" ;
		print "<td class='ll_$status' valign='top' id='td_$k'><b id='title_$k'>" . $title . "</b><br/>" ;
		if ( false === $exists ) print "<input type='text' size='50' id='newtitle_$k' value='$title' />" ;
		else {
			print "IMAGE IS ALREADY ON COMMONS<br/>(as {$exists->img_name})" ;
		}
		print "</td>" ;
		print "</tr>" ;
	}
	print "</table>" ;
	print "<a href='#' onclick='set_cb(0);return false'>Check all</a> | " ;
	print "<a href='#' onclick='set_cb(1);return false'>Uncheck all</a> | " ;
	print "<a href='#' onclick='set_cb(2);return false'>Toggle all</a>" ;
	print "<br/><input type='button' onclick='do_transfer_selected($cnt);' value='Transfer selected files' />" ;
}

function load_images_from_category () {
	global $category , $depth , $language , $project , $images , $limit ;
	$mysql_con = db_get_con_new($language,$project) ;
	$db = $language . 'wiki_p' ;
	$imgs = db_get_images_in_category ( $language , $category , $depth , $project ) ;
	while ( count ( $imgs ) > $limit ) array_pop ( $imgs ) ;
	while ( count ( $imgs ) > 0 ) {
		$i2 = array () ;
		while ( count ( $i2 ) < 50 && count ( $imgs ) > 0 ) $i2[] = array_pop ( $imgs ) ;
		$sql = "SELECT * FROM image WHERE img_name IN (\"" . implode ( '","' , $i2 ) . '")' ;
		$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
		while ( $o = mysql_fetch_object ( $res ) ) {
			$images[$o->img_name] = $o ;
		}
	}
}

function parse_file_list () {
	global $files , $images , $language , $project ;
	$fs = explode ( "\n" , $files ) ;
	foreach ( $fs AS $f ) {
		$f = preg_replace ( '/[\[\]]/' , '' , $f ) ;
		$f = preg_replace ( '/^[#*:\s]+/' , '' , $f ) ;
		$f = preg_replace ( '/^[a-z-]+:/' , '' , $f ) ;
		$f = preg_replace ( '/^(file|image|datei|bild):/i' , '' , $f ) ;
		$f = trim ( $f ) ;
		if ( $f == "" ) continue ;
		$f = ucfirst ( $f ) ;
//		$o = '' ;
//		$o->img_name = $f ;
		$images[$f] = db_get_image_data ( $f , $language , $project ) ;
	}
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$language = get_request ( "language" , "en" ) ;
$project = get_request ( "project" , "wikipedia" ) ;


print "<html><body>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' ;

print '<script type="text/javascript">' ;
print "var language='$language';\nvar project='$project';\n" ;
print "</script>" ;

?>

<style type='text/css'>
.form_table { border:1px solid black;background-color:#CCCCCC }
.result_header { border-bottom:2px solid grey;background-color:#EEEEEE }
.main_div { margin-left:5px;margin-right:5px;margin-bottom:15px;margin-top:0px }
.main_th { border-bottom:2px solid black;background-color:#CCCCCC }
.ll_unknown { border-bottom:1px solid grey;background-color:#FFFFCC }
.ll_commons { border-bottom:1px solid grey;background-color:#CCCCFF }
.ll_good { border-bottom:1px solid grey;background-color:#CCFFCC }
.ll_bad { border-bottom:1px solid grey;background-color:#FFCCCC }
.flickr { border-bottom:1px solid grey;background-color:#DDCCDD }
.gimp { border-bottom:1px solid grey;background-color:#DDDDCC }
.no_language_links { color:white; background-color:red; border:1px solid black; padding-left:5px; padding-right:5px }
.black_bottom { border-bottom:1px solid black }
.black_top { border-top:1px solid black }
.gray_bottom { border-bottom:1px solid gray }
</style>

<script type="text/javascript" src="./commcomm/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">

var proc_busy = 0 ;

$.postJSON2 = function (url, data, callback) {
    $.ajax({
        'url': url,
        'type': 'post',
        'processData': false,
        'data': JSON.stringify(data),
        contentType: 'application/json',
        success: function (data) { callback(JSON.parse(data)); },
    });
};


function set_cb ( p ) {
	$('.cbtransfer').each( function () {
		if ( p == 0 ) $(this).attr('checked',1) ;
		else if ( p == 1 ) $(this).attr('checked',0) ;
		else if ( p == 2 ) $(this).attr('checked',$(this).is(':checked')?0:1) ;
	} ) ;
}

function transfer_file ( id , title , newtitle ) {
	proc_busy++ ;
//	$('body').css('cursor','wait');
	$('#td_'+id).append ( '<div id="proc_' + id + '"><i>Processing...</i></div>' ) ;
	var data = {
		'newname' : newtitle ,
		'image' : title ,
		'language' : $('#language').val() ,
		'tusc_user' : $('#tusc_user').val() ,
		'tusc_password' : $('#tusc_password').val() ,
		'commonsense' : 1 ,
		'reallydirectupload' : 1 ,
		'doit' : 1
	} ;
	$.post ( './commonshelper.php' , data , function ( data ) {
		$('#td_'+id).css('background-color','#DDDDDD');
		$('#proc_'+id).remove();
		$('#td_'+id).append ( '<div><i>Processed</i></div>' ) ;
		$('#cb_'+id).remove();
		proc_busy-- ;
//		if ( proc_busy == 0 ) $('body').css('cursor','normal');
	} ) ;
}

function do_transfer_selected ( nof ) {
	proc_busy = 0 ;	
	$('.cbtransfer').each ( function () {
		if ( !$(this).is(':checked') ) return ;
		var id = this.id.substr ( 3 ) ;
		var title = $('#title_'+id).text() ;
		var newtitle = $('#newtitle_'+id).val() ;
		transfer_file ( id , title , newtitle ) ;
	} ) ;
}

</script>



<?PHP

print '</head>' ;
print get_common_header ( "pushforcommons.php" ) ;
print "<h1>Push for Commons (V2)</h1>" ;

$category = get_request ( "category" , "" ) ;
$depth = get_request ( "depth" , "0" ) ;
$files = get_request ( "files" , "" ) ;
$user_files = get_request ( "user_files" , "" ) ;
$cs2_url = get_request ( "cs2_url" , "" ) ;
$tusc_user = get_request ( "tusc_user" , "" ) ;
$tusc_password = get_request ( "tusc_password" , "" ) ;
$limit = 20 ;
$images = array () ;

print "<form method='post'>" ;
show_main_form () ;
if ( isset ( $_REQUEST['doit_look'] ) ) {
	if ( $category != '' ) load_images_from_category () ;
	else if ( $user != '' ) load_images_from_user () ;
	else if ( $files != '' ) parse_file_list () ;
	else print "<p><b>Need to use either category or user name!</b></p>" ;
	show_images () ;
}
print "</form>" ;

print "</body></html>" ;

?>