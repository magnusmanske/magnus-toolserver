<?PHP


# ______________________________________________________________________
# BEGIN OF CLASS ImageData

class ImageData {
	var $templates = array () ;
	var $title , $project , $language ;
	var $width , $height ;
	var $temporary_file ;
	var $metadata ;
	var $maybe_commons ;
	var $has_commons_tag ;
	var $possible_licenses ;
	
	# CONSTRUCTOR
	
	function ImageData ( $t = '' , $l = 'de' , $p = 'wikipedia' ) {
		$this->init () ;
		$this->fix_language_and_project ( $l , $p ) ;
		$this->title = trim ( str_replace ( '_' , ' ' , $t ) ) ;
		$this->language = $l ;
		$this->project = $p ;
	}

	# PUBLIC FUNCTIONS
	
	function make_random_image ( $l = 'de' , $p = 'wikipedia' ) {
		$this->init () ;
		$this->fix_language_and_project ( $l , $p ) ;
		$this->clean_temporary_file () ;
		do {
			$this->title = get_random_title_from_db ( $l , $p , '6' ) ;
#			$this->title = get_random_title ( $l , $p , "Image" ) ;
		} while ( $this->title === false ) ;
		$this->title = array_pop ( explode ( ':' , $this->title , 2 ) ) ; # Remove leaging "Image:"
		$this->language = $l ;
		$this->project = $p ;
	}

	function has_template ( $template ) {
		$template = strtolower ( trim ( $template ) ) ;
		foreach ( $this->templates AS $t ) {
			if ( strtolower ( trim ( $t ) ) == $template ) return true ;
		}
		return false ;
	}

	function get_width () {
		if ( $this->width == -1 ) $this->access_dimensions () ;
		return $this->width ;
	}

	function get_height () {
		if ( $this->height == -1 ) $this->access_dimensions () ;
		return $this->height ;
	}

	function get_url () {
		if ( $this->title == '' ) return '' ;
		return get_image_url ( $this->language , utf8_decode ( $this->title ) , $this->project ) ;
	}
	
	function get_description () {
		if ( !$this->load_metadata() ) return false ; # Something's wrong
		return $this->metadata->text ;
	}
	
	function get_desc_url ( $action = '' ) {
		$ret = get_wikipedia_url ( $this->language , 'Image:' . $this->title , $action , $this->project ) ;
		return $ret ;
	}
	
	function get_link ( $action = '' ) {
		return "<a target='_blank' href='" . $this->get_desc_url ( $action ) . "'>{$this->title}</a>" ;
	}
	
	function get_thumbnail_link ( $maxw = 120 , $maxh = 120 , $link_to_description = true ) {
		$img = $this->get_thumbnail ( $maxw , $maxh , 1 ) ;
		if ( $img == '' ) return '' ; # Something's wrong
		if ( $link_to_description ) $href = $this->get_desc_url() ;
		else $href = $this->get_url() ;
		return "<a target='_blank' href='{$href}'>{$img}</a>" ;
	}
	
	function get_thumbnail ( $maxw = 120 , $maxh = 120 , $border = 0 ) {
		# Return dummy icon for non-image files
		if ( !$this->is_image() ) {
			return "<img border='{$border}' src='http://commons.wikimedia.org/skins-1.5/common/images/icons/fileicon-ogg.png'/>" ;
		}
		
		if ( !$this->access_dimensions () ) return '' ; # Something's wrong
		
		# Normal image, continue
		$w = $this->width ;
		$h = $this->height ;
		if ( $w > $maxw ) {
			$h = $h * $maxw / $w ;
			$w = $maxw ;
		}
		if ( $h > $maxh ) {
			$w = $w * $maxh / $h ;
			$h = $maxh ;
		}
		$w = round ( $w ) ;
		$h = round ( $h ) ;
		return "<img border='{$border}' width='{$w}' height='{$h}' src='" . $this->get_thumbnail_url($w) . "'/>" ;
	}
	
	function try_tumbnail_url ( $width ) {
		$image = $this->title ;
		$image2 = ucfirst ( str_replace ( " " , "_" , $this->title ) ) ;
		$m = md5( $image2 ) ;
		$m1 = substr ( $m , 0 , 1 ) ;
		$m2 = substr ( $m , 0 , 2 ) ;
		
		$url = "http://upload.wikimedia.org/wikipedia/{$this->language}/thumb/{$m1}/{$m2}/" . myurlencode ( $image ) ;
		$url .= '/' . $width . 'px-' . myurlencode ( $image ) ;
		
#		print $url . "<br/>" ;
		return $url ;
	}
	
	function get_thumbnail_url ( $width ) {
		$url = $this->try_tumbnail_url ( $width ) ;
		return $url ;
	}
	
	function cleanup () {
		$this->clean_temporary_file() ;
	}
	
	function check_license () {
		global $good_templates , $evil_templates , $ignore_templates ;
		$text = $this->get_description () ;
		if ( $text === false ) return 'unknown' ;
		$text = strtolower ( $text ) ;
		$text = str_replace ( '{{msg:' , '{{' , $text ) ;
		$text = str_replace ( '{{vorlage:' , '{{' , $text ) ;
		$text = str_replace ( '{{plantilla:' , '{{' , $text ) ;
		$text = str_replace ( '{{mod�le:' , '{{' , $text ) ;
		#print $text . "<hr/>" ;
		foreach ( $good_templates AS $t ) {
			$t = '{{' . strtolower ( $t ) ;
			if ( false !== strpos ( $text , $t ) ) return 'good' ;
		}
		foreach ( $evil_templates AS $t ) {
			$t = '{{' . strtolower ( $t ) ;
			if ( false !== strpos ( $text , $t ) ) return 'bad' ;
		}
		foreach ( $ignore_templates AS $t ) {
			$t = '{{' . strtolower ( $t ) ;
			if ( false !== strpos ( $text , $t ) ) return 'ignore' ;
		}
		return 'maybe' ;
	}

	function is_equal_to ( &$i ) {
		if ( $this->get_width() <= 0 || $this->get_height() <= 0 ) return false ; # Can't compare size
		if ( $this->get_width() != $i->get_width() ) return false ;
		if ( $this->get_height() != $i->get_height() ) return false ;
		if ( @filesize ( $this->temporary_file ) != @filesize ( $i->temporary_file ) ) return false ;
		return true ;
	}
	
	function get_file_type () {
		$ret = trim ( strtolower ( array_pop ( explode ( '.' , $this->title ) ) ) ) ;
		if ( $ret == 'jpeg' ) $ret = 'jpg' ;
		if ( $ret == 'tiff' ) $ret = 'tif' ;
		return $ret ;
	}
	
	function is_image () {
		$type = $this->get_file_type () ;
		if ( $type == 'jpg' ) return true ;
		if ( $type == 'png' ) return true ;
		if ( $type == 'gif' ) return true ;
		if ( $type == 'tif' ) return true ;
		return false ;
	}
	
	function is_sound_file () {
		$type = $this->get_file_type () ;
		if ( $type == 'mid' ) return true ;
		if ( $type == 'ogg' ) return true ;
		if ( $type == 'mp3' ) return true ;
		if ( $type == 'wav' ) return true ;
		return false ;
	}
	
	function get_edit_button ( $button_label , $edit_summary , $edit_text , $add = true ) {
		$this->load_metadata () ;
		$timestamp = $this->metadata->clean_timestamp ;		
		$starttime = date ( "YmdHis" , time() + (12 * 60 * 60) ) ;
		$url = "http://{$this->language}.{$this->project}.org/w/index.php?title=Image:" . myurlencode ( $this->title ) . '&action=edit' ;
		
		if ( $add ) $edit_text = $this->get_description() . $edit_text ;
		$edit_text = trim ( mask_quotes ( $edit_text ) ) ;
		$edit_summary = trim ( mask_quotes ( $edit_summary ) ) ;

		$button = "<form id='upload' method=post enctype='multipart/form-data' action='{$url}' target='_blank' style='display:inline'>" ;
		$button .= "<input type='hidden' name='wpTextbox1' value='" . $edit_text . "'/>" ;
		$button .= "<input type='hidden' name='wpSummary' value='{$edit_summary}'/>" ;
		$button .= "<input type='hidden' name='wpPreview' value='wpPreview' />" ;
		$button .= "<input type='hidden' value='{$starttime}' name='wpStarttime' />" ;
		$button .= "<input type='hidden' value='{$timestamp}' name='wpEdittime' />" ;
		$button .= "<input type='submit' name='wpPreview' value='{$button_label}'/>" ;
		$button .= "</form>" ;
		return $button ;
	}
	
	function get_commons_upload_button ( $button_label , $directupload = false ) {
		if ( $this->language == 'commons' ) return '' ; # Don't upload commons images to commons ;-)
		$ret = "<form style='display:inline' method='post' target='_blank' enctype='multipart/form-data' action='commonshelper.php'>" .
			  "<input type='hidden' name='lang' value='{$this->language}' />" .
			  "<input type='hidden' name='image' value='{$this->title}' />" .
			  "<input type='hidden' name='commonsense' value='1' />" ;
		if ( $directupload ) $ret .= "<input type='hidden' name='directupload' value='1' />" ;
		$ret .= "<input type='submit' name='doit' value='{$button_label}' /></form>" ;
		return $ret ;
	}
	
	function get_delete_link ( $reason , $link_label = 'Delete' ) {
		$url1 = myurlencode ( 'Image:' . $this->title ) ; ;
		$url2 = myurlencode ( $this->title ) ;
		$url = "http://{$this->language}.{$this->project}.org/w/index.php?title={$url1}&image={$url2}&action=delete" ;
		$ret = "<a target='_blank' href=\"{$url}&wpReason={$reason}\">{$link_label}</a>" ;
		return $ret ;
	}
	

	# PRIVATE FUNCTIONS
	
	function init () {
		$this->width = -1 ;
		$this->height = -1 ;
		$this->metadata = '' ;
		$this->metadata->ok = false ;
		$this->maybe_commons = false ;
		$this->has_commons_tag = false ;
		$this->temporary_file = '' ;
		$this->possible_licenses = array () ;
	}
	
	function load_metadata () {
		if ( $this->title == '' ) return false ; # No title
		if ( $this->metadata->ok ) return true ; # Already loaded
		$this->metadata = get_page_object ( "Image:" . $this->title , $this->project , $this->language ) ;
		if ( $this->metadata->ok ) {
			$this->check4commons() ;
			$this->guess_license() ;
			$this->extract_templates() ;
		}
		return $this->metadata->ok ;
	}
	
	function check4commons () {
		$this->maybe_commons = false ;
		$ldesc = strtolower ( $this->get_description() ) ;
		
		# "Commons" mentioned?
		$ldesc2 = str_replace ( 'creative commons' , '' , $ldesc ) ; # So it doesn't find "commons"
		if ( false !== strpos ( $ldesc2 , "commons" ) ) {
			$this->maybe_commons = true ;
		}

		# Commons template?
		$this->has_commons_tag = false ;
		if ( false !== strpos ( $ldesc , "{{nc" ) ) $this->has_commons_tag = true ;
		if ( false !== strpos ( $ldesc , "{{shadowscommons" ) ) $this->has_commons_tag = true ;
		if ( false !== strpos ( $ldesc , "{{nowcommons" ) ) $this->has_commons_tag = true ;
	}
	
	function guess_license () {
		global $license_indicators , $language_image_licenses ;
		$this->possible_licenses = array () ;

		$ldesc = strtolower ( $this->get_description() ) ;
		$ldesc = str_replace ( ' ' , '' , $ldesc ) ;
		$ldesc = str_replace ( '-' , '' , $ldesc ) ;
		$ldesc = str_replace ( '/' , '' , $ldesc ) ;
		
		foreach ( $license_indicators AS $indicator => $license ) {
			if ( false === strpos ( $ldesc , $indicator ) ) continue ; # Indicator not in description
			
			# Localization?
			$ol = $this->language . ':' . strtolower ( $license ) ;
			if ( isset ( $language_image_licenses[$ol] ) ) $license = $language_image_licenses[$ol] ;
			
			if ( in_array ( $license , $this->possible_licenses ) ) continue ; # We already have that one
			$this->possible_licenses[] = $license ; # Adding new possible license
		}
	}

	function fix_language_and_project ( &$l , &$p ) {
		if ( $p == 'commons' ) $l = 'commons' ;
		if ( $l == 'commons' ) $p = 'wikipedia' ;
	}
	
	function clean_temporary_file () {
		if ( $this->temporary_file == '' ) return ;
		@unlink ( $this->temporary_file  ) ;
		$this->temporary_file = '' ;
	}
	
	function extract_templates () {
		$this->templates = array () ;
		$t = $this->get_description () ;
		if ( $t === false ) return ;
		$t = explode ( '{{' , " {$t}" ) ;
		array_shift ( $t ) ; # Before first {{
		foreach ( $t AS $s ) {
			$s = array_shift ( explode ( '}}' , $s , 2 ) ) ;
			$s = array_shift ( explode ( '|' , $s , 2 ) ) ;
			$this->templates[] = ucfirst ( trim ( $s ) ) ;
		}
	}
	
	function copy_to_temporary_file () {
		if ( $this->temporary_file != '' ) return true ;
		$this->temporary_file = tempnam ( "/tmp" , "ict" ) ;
		if ( !@copy ( $this->get_url() , $this->temporary_file ) ) {
			$this->temporary_file = '' ;
			return false ;
		}
		return true ;
	}
	
	function access_dimensions () {
		if ( $this->width != -1 && $this->height != -1 ) return true ;
		$this->copy_to_temporary_file() ;
		$this->width = 0 ;
		$this->height = 0 ;
		if ( $this->temporary_file == '' ) return false ;
		image_dimensions ( $this->temporary_file , $this->width , $this->height , 0 ) ;
		return true ;
	}
}

# END OF CLASS ImageData
# ______________________________________________________________________


?>