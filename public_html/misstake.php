<?php

error_reporting ( E_ALL ) ;
require_once ( "php/common.php" ) ;
include_once ( "php/wikiquery.php") ;

function db_get_page_id ( $db , $title , $ns = 0 ) {
	make_db_safe ( $ns ) ;
	make_db_safe ( $title ) ;
	$sql = "SELECT page_id FROM page WHERE page_title=\"$title\" AND page_namespace=$ns" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	if($o = $result->fetch_object()){
		return $o->page_id ;
	}
	return -1 ;
}


$language = get_request ( 'language' , 'en' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;
$page = get_request ( 'page' , '' ) ;

print get_common_header ( "misstake.php" , 'Ranked missing articles' ) ;

print "
<form method='post'>
<table class='table-condensed'>
<tr><th>Project</th><td colspan=2><input name='language' value='$language' />.<input name='project' value='$project' /></td></tr>
<tr><th>Page(s)</th><td><textarea name='page' value='$page' cols=60 rows=3>$page</textarea></td><td>All namespaces allowed<br/>One page per line</td></tr>
<tr><th></th><td><input name='doit' class='btn btn-primary' value='Do it' type='submit' /></td></tr>
</table>
</form>
" ;

if ( !isset ( $_REQUEST['doit'] ) ) {
	print get_common_footer() ;
	exit ( 0 ) ;
}


if ( $page != '' ) {
	$wq = new WikiQuery ( $language , $project ) ;
	$db = openDB ( $language , $project ) ;
//	$mysql_con = db_get_con_new ( $language , $project ) ;
//	$db = get_db_name ( $language , $project ) ;

	$nss = $wq->get_namespaces() ;
//	print "<pre>" ; print_r ( $nss ) ; print "</pre>" ;

	$pageids = array() ;
	$pagelist = explode ( "\n" , $page ) ;
	foreach ( $pagelist AS $page ) {
		$page = str_replace ( '_' , ' ' , $page ) ;
		$ns = 0 ;
		foreach ( $nss AS $k => $v ) {
			$n = strtoupper ( $v . ':' ) ;
			if ( strtoupper ( substr ( $page , 0 , strlen ( $n ) ) ) == $n ) {
				$ns = $k ;
				$page = substr ( $page , strlen ( $n ) ) ;
				break ;
			}
		}
		
		if ( $ns == 14 ) { // Category
			make_db_safe ( $page ) ;
			$sql = "SELECT $slow_ok_limit DISTINCT page_title,page_namespace FROM page,categorylinks WHERE page_id=cl_from AND cl_to=\"{$page}\"" ;
			if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
			while($o = $result->fetch_object()){
//				print "<div>Adding " . $o->page_title . "</div>" ;
				$pid = db_get_page_id ( $db , $o->page_title , $o->page_namespace ) ;
				$pageids[$pid] = $pid ;
			}
		} else {
			$pid = db_get_page_id ( $db , $page , $ns ) ;
			$pageids[$pid] = $pid ;
		}
	}
	print "<div>Checking " . count ( $pageids ) . " pages for redlinks and their \"wantedness\"...</div>" ; myflush() ;
	if ( count ( $pageids ) > 1 ) {
		$pageid = implode ( ',' , $pageids ) ;
		$pageid = " IN ( $pageid ) " ;
	} else {
		$pageid = implode ( ',' , $pageids ) ;
		$pageid = " = $pageid " ;
	}

#	print "$pageid : $ns / $language , $project , $page<br/>" ;
	$sql = "select p1.pl_title AS title,count(*) as cnt from pagelinks p1,pagelinks p2 where p1.pl_namespace=0 and p1.pl_title=p2.pl_title and p2.pl_from $pageid and p2.pl_namespace=0 and not exists ( select * from page where page_title=p2.pl_title and page_namespace=0) group by p1.pl_title order by cnt desc" ;
#	print $sql ;

	print "<table>" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$t = str_replace ( '_' , ' ' , $o->title ) ;
		print "<tr>" ;
		print "<td><a href='http://$language.$project.org/wiki/Special:WhatLinksHere/" . urlencode($o->title) . "'>" . $t . "</a></td>" ;
		print "<td>" . $o->cnt . "</td>" ;
		print "</tr>" ;
	}
	print "</table>" ;
}

print get_common_footer() ;
?>