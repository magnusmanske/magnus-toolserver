<?PHP

$script = "bibcomm.php" ;

$field_keys['isbn'] = array ( 'Autor', 'Titel', 'ISBN', 'Herausgeber', 'TitelErg', 'Sammelwerk', 'Band', 'Nummer', 'Auflage', 'Verlag', 'Ort', 'Jahr', 'Monat', 'Tag', 'ISSN', 'LCCN',  'Originaltitel', 'Originalsprache', 'Übersetzer', 'Online', 'DOI', 'arxiv', 'Zugriff', 'Typ' ) ;
// 'Kapitel', 'Seiten', 'Spalten', 'ISBNistFormalFalsch', 

$essential_fields['isbn'] = array ( 'Autor' , 'Titel' , 'ISBN' ) ;

$dummy_fields['isbn'] = array ( 'Seite' => '{{{Seite|}}}' , 'Seiten' => '{{{Seiten|}}}' , 'Kommentar' => '{{{Kommentar|}}}' , 'format' => '{{{format|}}}' , 'record' => '{{{record|}}}' ) ;

$direct = isset ( $_REQUEST['direct'] ) ;
$postscript = '' ;

include "php/common.php" ;
include "php/legacy.php" ;



function get_between ( &$text , $k1 , $k2 ) {
	$t = array_pop ( explode ( $k1 , $text , 2 ) ) ;
	$t = array_shift ( explode ( $k2 , $t , 2 ) ) ;
	return trim ( $t ) ;
}

function prefill_google () {
	if ( !isset ( $_REQUEST['entry_isbn'] ) ) return ;
	$isbn = $_REQUEST['entry_isbn'] ;
	$ret = "" ;
	$ret->isbn = $isbn ;
	$isbn = trim ( str_replace ( '-' , '' , $isbn ) ) ;
	$url = "http://books.google.com/books/feeds/volumes?q=isbn:$isbn&max-results=25" ;

	$text = @file_get_contents ( $url ) ;
#	print "Testing google search... $url<hr/>" . htmlentities ( $text ) . "<hr/>" ;
	$entries = explode ( '<entry' , $text ) ;

	if ( count ( $entries ) != 2 ) return $ret ;

	$e = array_pop ( $entries ) ;
	
	$au = explode ( '<dc:creator' , $e ) ;
	if ( count ( $au ) > 1 ) {
		array_shift ( $au ) ;
		foreach ( $au AS $a ) {
			$ret->author[] = get_between ( $a , '>' , '</dc:creator>' ) ;
		}
	}
	
	$ret->date = get_between ( $e , '<dc:date>' , '</dc:date>' ) ;
	if ( $ret->date != '' ) {
		$ret->year = substr ( $ret->date , 0 , 4 ) ;
	}
	
	$t2 = explode ( '<dc:title' , $e ) ;
	if ( count ( $t2 ) > 1 ) {
		array_shift ( $t2 ) ;
		$title = '' ;
		foreach ( $t2 AS $t3 ) {
			$t = get_between ( $t3 , '>' , '</dc:title>' ) ;
			if ( substr ( $t , 0 , 1 ) == '[' ) continue ;
			$title .= $t . '. ' ;
		}
		$ret->title = trim ( $title , " ." ) ;
	}
	
	if ( count ( explode ( '<dc:publisher>' , $e ) ) == 2 ) {
		$ret->publisher = get_between ( $e , '<dc:publisher>' , '</dc:publisher>' ) ;
	}
	
#	$ret->place = explode ( ";" , array_shift ( explode ( ":" , $data['verleger'] , 2 ) ) ) ;
	$ret->source = "google" ;

/*	print "<pre>" ;
	print_r ( $ret ) ;
	print "</pre>" ;*/
	
	if ( isset ( $ret->year ) ) $_REQUEST['entry_jahr'] = $ret->year ;
	if ( isset ( $ret->title ) ) $_REQUEST['entry_titel'] = $ret->title ;
	if ( isset ( $ret->author ) ) {
		$wq = new WikiQuery ( 'de' , 'wikipedia' ) ;
		$au = array () ;
		foreach ( $ret->author AS $a ) {
			$existing = $wq->get_existing_pages ( array ( $a ) ) ;
			if ( count ( $existing ) == 0 ) $au[] = $a ;
			else $au[] = "[[$a]]" ;
		}
		$_REQUEST['entry_autor'] = implode ( ', ' , $au ) ;
	}
}




function prettify_isbn ( $isbn ) {
	$i = preg_replace ( '/[^\dxX]/' , '' , $isbn ) ;
	if ( $i == '' ) return $isbn ;
	$t = file_get_contents ( "http://toolserver.org/gradzeichen/IsbnCheckAndFormat?ISBN=$i&FormatOnlyRaw=yes" ) ;
	if ( substr ( $t , 0 , 3 ) == 'Not' ) return $isbn ;
	return $t ;
}

if ( isset ( $_REQUEST['entry_isbn'] ) ) {
	$_REQUEST['entry_isbn'] = prettify_isbn ( $_REQUEST['entry_isbn'] ) ;
}

$mode = get_request ( 'mode' , '' ) ;
$type = get_request ( 'type' , '' ) ;

print "<html><head></head><body" ;
if ( $direct ) {
	$onload = true ;
	if ( $type == 'isbn' and $mode == 'add' ) {
		if ( '' == get_request('entry_autor','') ) $onload = false ;
		if ( '' == get_request('entry_titel','') ) $onload = false ;
		if ( '' == get_request('entry_isbn','') ) $onload = false ;
	}
	if ( $onload ) print " onload=\"document.getElementsByTagName('form')[0].submit()\"" ;
}
print ">" ;
print get_common_header ( $script , "BibCommons" ) ;
print "<h1>BibCommons</h1>" ;


function parse_Literatur ( $lit ) {
	$lit = explode ( '}}' , $lit , 2 ) ;
	$lit = explode ( '|' , array_shift ( $lit ) ) ;
	array_shift ( $lit ) ;
	foreach ( $lit AS $l ) {
		$l = trim ( $l ) ;
		$l = explode ( '=' , $l , 2 ) ;
		$k = trim ( $l[0] ) ;
		$v = trim ( $l[1] ) ;
		$k = 'entry_' . strtolower ( $k ) ;
		if ( !isset ( $_REQUEST[$k] ) ) $_REQUEST[$k] = $v ;
	}
}

function show_entry_form ( $warn ) {
	global $type , $field_keys , $essential_fields ;
	print "<form method='post' action='$script'>" ;

	$dt = get_request ( 'dummytext' , '' ) ;
	if ( $dt != '' ) {
		if ( preg_match ( '/\{\{Literatur/' , $dt ) ) parse_Literatur ( $dt ) ;
		$dt = str_replace ( ';' , "\n" , $dt ) ;
		$dt = str_replace ( ':' , "\n" , $dt ) ;
		$dt = str_replace ( "\n " , "\n" , $dt ) ;
		$dt = trim ( $dt ) ;
		print "Basisreferenz (zum Rauskopieren):<textarea rows='4' style='width:100%'>$dt</textarea><br/>" ;
	}

	print "<table border=1>" ;
	foreach ( $field_keys[$type] AS $value ) {
		print "<tr><td>" ;
		$ev = false ;
		if ( in_array ( $value , $essential_fields[$type] ) ) {
			print "<b>$value</b>" ;
			$ev = true ;
		} else {
			print $value ;
		}
		print "</td>" ;
		$v = 'entry_' . strtolower ( $value ) ;
		$ov = get_request ( $v , '' ) ;
		$w = '' ;
		if ( $ev and $warn and $ov == '' ) $w = " <span style='color:red'>Dieses Feld muss ausgefüllt werden!</span>" ;
		print "<td><input type='text' name='$v' value='$ov' size=50 />$w</td>" ;
		print "</tr>" ;
	}
	print "</table>" ;
	print "<input type='hidden' name='mode' value='add' />" ;
	print "<input type='hidden' name='type' value='$type' />" ;
	print "<input type='submit' name='doit' value='Eintrag erstellen' />" ;
	print "</form>" ;
}

function seed_from_db ( $id ) {
	global $type ;
	$mysql_con = db_get_con () ;
	make_db_safe ( $id ) ;
	$db = 'u_magnus_yarrow' ;
	$sql = "SELECT * FROM bibrec WHERE bibrec='$id'" ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	$entry = array () ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$entry[$o->tag] = $o->value ;
//		print $o->tag . " : " . $o->value . "<br/>" ;
	}
	if ( count ( $entry ) == 0 ) return false ;
	$type = 'isbn' ;
	foreach ( $entry AS $k => $v ) {
		$k = 'entry_' . strtolower ( $k ) ;
		$v = str_replace ( '@' , '' , $v ) ;
		$_REQUEST[$k] = $v ;
	}
	return true ;
}

function create_wikitext () {
	$ret = '' ;
	global $type , $field_keys , $essential_fields , $dummy_fields ;
	if ( $type == 'isbn' ) $ret .= "{{BibRecord\n" ;
	$a = array () ;
	foreach ( $field_keys[$type] AS $value ) {
		$v = 'entry_' . strtolower ( $value ) ;
		$ov = get_request ( $v , '' ) ;
		$a[$value] = $ov ;
	}
	foreach ( $dummy_fields[$type] AS $k => $v ) $a[$k] = $v ;
	$max_space = 0 ;
	foreach ( $a AS $k => $v ) {
		if ( $max_space < strlen ( $k ) ) $max_space = strlen ( $k ) ;
	}
	foreach ( $a AS $k => $v ) {
		$sp = '' ;
		for ( $n = strlen ( $k ) ; $n < $max_space ; $n++ ) $sp .= ' ' ;
		$ret .= "| $k$sp = $v\n" ;
	}
	$ret .= '}}' ;
	return $ret ;
}

if ( $mode == '' ) {
	print "<h2>Neuer Eintrag</h2>" ;
	print "<ul>" ;
	foreach ( $field_keys AS $type => $a ) {
		print "<li><a href='$script?mode=new&type=$type'>" . ucfirst ( $type ) . "</a></li>" ;
	}
	
	print "<li>ISBN <form style='display:inline' action='bibcomm.php' method='get'>" ;
	print "<input type='text' name='entry_isbn' value='' />" ;
	print "<input type='submit' name='doit' value='Google-prefill' />" ;
	print "<input type='hidden' name='prefill' value='google' />" ;
	print "<input type='hidden' name='mode' value='new' />" ;
	print "<input type='hidden' name='type' value='isbn' />" ;
	print "</form></li>" ;
	
	
	print "<li>ID <form style='display:inline' action='bibcomm.php' method='get'>" ;
	print "<input type='text' name='id' value='' />" ;
	print "<input type='submit' name='doit' value='Aus Datenbank importieren' />" ;
	print "<input type='hidden' name='mode' value='new' />" ;
	print "<input type='hidden' name='type' value='fromdb' />" ;
	print "</form></li>" ;
	print "<ul>" ;
} else if ( $mode == 'new' and $type == 'fromdb' ) {
	$id = $_REQUEST['id'] ;
	if ( seed_from_db ( $id ) ) {
		print "<h2>Neuer Eintrag</h2>" ;
		show_entry_form ( false ) ;
	} else {
		print "$id nicht in der Datanbank" ;
	}
} else if ( $mode == 'new' ) {
	print "<h2>Neuer Eintrag</h2>" ;
	if ( 'google' == get_request ( 'prefill' ) ) prefill_google () ;
	show_entry_form ( false ) ;
} else if ( $mode == 'add' ) {
	$all_essentials = true ;
	foreach ( $essential_fields[$type] AS $ef ) {
		$v = 'entry_' . strtolower ( $ef ) ;
		$ov = get_request ( $v , '' ) ;
		if ( $ov != '' ) continue ;
		$all_essentials = false ;
		break ;
	}
	if ( $all_essentials ) {
		$wiki = create_wikitext () ;
		
		$title = '' ;
		if ( $type == 'isbn' ) {
			$title = get_request ( 'entry_isbn' ) ;
			$title = preg_replace ( '/[^\dxX]/' , '' , $title ) ;
			$title = "Vorlage:BibISBN/$title" ;
		}
		
		if ( $title != '' ) {
			$wq = new WikiQuery ( 'de' , 'wikipedia' ) ;
			$existing = $wq->get_existing_pages ( array ( $title ) ) ;
			if ( count ( $existing ) > 0 ) {
				print "<b><a href='http://de.wikipedia.org/wiki/$title'>$title</a> existiert schon!</b>" ;
				exit ;
			}
		}

		
		if ( $direct ) print "Wird weitergeleitet (JavaScript erforderlich)...<div style='display:none'>" ;
		print "<textarea style='width:100%' rows=20>$wiki</textarea>" ;

		if ( $title != '' ) {
			$button = cGetEditButton ( $wiki , $title , 'de' , 'wikipedia' , 'Neuer Eintrag' , 'Eintrag auf Wikipedia anlegen' , false , false , false , true , -1 , true ) ;
			print $button ;
			print "<a href='http://de.wikipedia.org/w/index.php?title=$title&action=edit&redlink=1'>$title</a>" ;
			if ( !$direct ) $postscript = "<script>window.onload=function(){document.getElementsByTagName('form')[0].submit()}</script>" ;
		}
		if ( $direct ) print "</div>" ;
		
	} else {
		print "<h2>Neuer Eintrag</h2>" ;
		show_entry_form ( true ) ;
	}
}

print "$postscript</body></html>" ;

?>