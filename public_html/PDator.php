<?PHP

include "common.php" ;

function db_get_articles_in_category_subset ( $language , $category , $depth = 0 , $namespace = 0 , &$done_cats = array () , $no_redirects = false , $limit = '' , $project = 'wikipedia' , $only_redirects = false , $subset = array () ) {
	if ( in_array ( $category , $done_cats ) ) return array () ;
	$mysql_con = db_get_con_new($language,$project) ;
	$db = get_db_name ( $language , $project ) ;
	make_db_safe ( $category ) ;
	if ( $limit != '' ) $limit = "LIMIT $limit" ;
	$limit = str_replace ( 'LIMIT LIMIT' , 'LIMIT' , $limit ) ; // Some odd bug
	
	$ret = array () ;
	$subcats = array () ;
	$red = $no_redirects ? ' AND page_is_redirect=0' : '' ;
	if ( $only_redirects ) $red = ' AND page_is_redirect=1' ;
	$sql = "SELECT $slow_ok_limit ".get_tool_name()." page_title,page_namespace FROM page,categorylinks WHERE page_id=cl_from AND cl_to=\"{$category}\" $red $limit" ;
//	print "TESTING : $depth - $category : $sql<br/>" ;
	
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		
		if ( !isset ( $o->page_namespace ) ) continue ;
		if ( $o->page_namespace == 14 AND $depth > 0 ) {
			$subcats[] = $o->page_title ;
			if ( $namespace >= 0 and $o->page_namespace != $namespace ) continue ;
		} else if ( $namespace >= 0 and $o->page_namespace != $namespace ) continue ;
		if ( isset ( $subset[$o->page_title] ) ) $ret[$o->page_title] = $o->page_title ; // SUBSET!
//		print "TESTING : $depth - $category / " . $o->page_title . "<br/>" ;
	}
	mysql_free_result ( $res ) ;

	$done_cats[] = $category ;
	foreach ( $subcats AS $sc ) {
//		print "Testing : $depth - $sc<br/>" ;
		$ret2 = db_get_articles_in_category_subset ( $language , $sc , $depth - 1 , $namespace , $done_cats , $no_redirects , $limit , $project , $only_redirects , $subset ) ;
		foreach ( $ret2 AS $k => $v ) $ret[$k] = $v ;
	}

	return $ret ;
}

function find_authors ( $d ) {
	global $pdus , $total_authors , $us_death_year , $year , $done ;
	$id = $d[0] ;
	$category = $d[1] ;
	$min_death = $d[2] ;
	$depth = $d[3] ;

	if ( isset ( $done[$id] ) ) return ;
	$done[$id] = 1 ;
	$death_year = $year - $min_death - 1 ;

	if ( $id == 'US' ) {
		$us_death_year = $death_year ;
	} else {
		if ( $pdus && $death_year > $us_death_year ) $death_year = $us_death_year ;
	}

	$dyc = "$death_year deaths" ;
//		print "$id : $dyc<br/>\n" ; myflush() ;
	
	$a = array () ;
	$deaths = db_get_articles_in_category ( 'en' , $dyc , 0 , 0 , $a , true , '' , 'wikipedia' , false ) ;


//		print "$id : $category<br/>\n" ; myflush() ;
	$a = array () ;
	$authors = db_get_articles_in_category_subset ( 'en' , $category , $depth , 0 , $a , true , '' , 'wikipedia' , false , $deaths ) ;
//		print "<pre>" ; print_r ( $authors ) ; print "</pre>" ;
	
	foreach ( $authors AS $a ) $total_authors[$a] = $a ;
	
	print "<tr><th>$category</th><td>$death_year</td><td>" . count ( $authors ) . "</td></th>" ; myflush() ;
}

$year = get_request ( 'year' , 2013 ) ;
$pdus = isset ( $_REQUEST['pdus'] ) ;
$pdus_c = $pdus ? 'checked' : '' ;

print '<html><head><title>PDator</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head><body>' ;
print get_common_header ( "PDator.php" , "PDator" ) ;

print "Determines that works of which authors will go into the public domain in a given year. See <a href='http://meta.wikimedia.org/wiki/PDator'>here</a> for the copyright data underlying this tool." ;
print "<form method='get'><table>" ;
print "<tr><th>Year</th><td><input type='number' name='year' value='$year' /></td></tr>" ;
print "<tr><th>PD US</th><td><input type='checkbox' id='year' name='pdus' value='1' $pdus_c /><label for='year'>Only those that will be public domain in the US as well</label></td></tr>" ;
print "<tr><th/><td><input type='submit' name='doit' value='Do it' /></td></tr>" ;
print "</table></form>" ;

if ( isset ( $_REQUEST['doit'] ) ) {
	
	
	$data = array ( ) ;
	$x = file_get_contents ( "http://meta.wikimedia.org/w/index.php?title=PDator&action=raw" ) ;
	$x = explode ( "\n" , $x ) ;
	foreach ( $x AS $y ) {
		$y = trim ( $y ) ;
		if ( substr ( $y , 0 , 1 ) != '|' ) continue ;
		$y = explode ( '||' , $y ) ;
		$d = array () ;
		foreach ( $y AS $z ) {
			$z = str_replace ( '|' , '' , $z ) ;
			$d[] = trim ( $z ) ;
		}
		if ( count ( $d ) < 4 ) continue ;
		$data[] = $d ;
	}
	
/*		array ( 'US' , 'American writers' , 70 , 9 ) ,
		array ( 'UK' , 'British writers' , 70 , 9 ) ,
		array ( 'DE' , 'German writers' , 70 , 9 )
	) ;*/
//print "<pre>" ; print_r ( $data ) ; print "</pre>" ; exit ( 0 ) ;
	$total_authors = array () ;
	print "<table border='1'><tr><th>Group</th><th>Year of death</th><th># Found</th></tr>" ;
	
	// Check US PD first, if necessary
	if ( $pdus ) {
		foreach ( $data AS $d ) {
			if ( $d[0] == 'US' ) find_authors ( $d ) ;
		}
	}
	
	foreach ( $data AS $d ) {
		find_authors ( $d ) ;
	}
	
	print "</table>" ;
	
	print "<h3>" . count ( $total_authors ) . " total authors found</h3><textarea style='width:100%' rows='20'>" ;
	foreach ( $total_authors AS $a ) {
		$a = str_replace ( '_' , ' ' , $a ) ;
		print "$a\n" ;
	}
	print "</textarea>" ;
}

print "</body></html>" ;

?>
