<?PHP

include_once ( "php/common.php" ) ;
error_reporting ( E_ALL ) ;

$modes = array (
'pedia' => 'Encyclopedia',
'image' => 'Images',
'av' => 'Audio/Video',
'news' => 'News',
'dict' => 'Dictionary',
'map' => 'Maps'
) ;


$mode = get_request ( 'mode' , 'pedia' ) ;
$q = trim ( urldecode ( get_request ( 'q' , '' ) ) ) ;

function print_header() {
//	print "<html>" ;
//	print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <script src="bong.js" type="text/javascript"></script></head>' ;
//	print "<body style='background:url(\"http://upload.wikimedia.org/wikipedia/commons/7/76/Ed_g2s_background.jpg\") 0% 0% no-repeat'>" ;
	$s = get_common_header ( "bong.php" , 'BONG' ) ;
	$s = str_replace ( '<body>' , "<body style='background:url(\"http://upload.wikimedia.org/wikipedia/commons/7/76/Ed_g2s_background.jpg\") 0% 0% no-repeat'>" , $s ) ;
	print $s . '<script src="bong.js" type="text/javascript"></script>' ;
}

function print_top_links() {
	global $mode , $modes ;
	print "<table cellspacing='0' cellpadding='2px'><tr>" ;
	foreach ( $modes AS $m => $t ) {
		if ( $mode == $m ) {
			print "<td style='font-weight: bold;border:1px solid white;background:#DDDDDD'>$t</td>" ;
		} else {
			$ta = "<a href='#' onclick='openmode(\"$m\")'>$t</a>" ;
			print "<td style='border:1px solid white'>$ta</td>" ;
		}
	}
	print "</tr></table>" ;
}

function print_search_box() {
	global $mode , $q ;
	print "<form method='get' action='javascript:openmode(\"$mode\")' class='form inline-form form-inline' >" ;
	print "<div style='vertical-align:middle'>" ;
	print "<input style='font-family:  Verdana, Arial, Helvetica, sans-serif;font-size:12pt' id='q' name='q' value='$q' size='50' /> " ;
	print "<input type='submit' name='doit' value='1' style='display:none' />" ;
	print "<a href='#' onclick='openmode(\"$mode\")'>" ;
	print "<img border='0' style='position:relative;top:8px' src='http://upload.wikimedia.org/wikipedia/commons/3/33/Crystal_Clear_action_viewmag.png' width='30px' /></a>" ;
	print "</div></form>" ;
}

function show_entry_form () {
	print_header () ;
	
	print "<div width='100%' style='margin-left:25%;margin-right:25%'>" ;
	
	print_top_links () ;
	print_search_box () ;
	
	print "</div>" ;

	if ( isset ( $_REQUEST['doit'] ) ) show_search_results () ;

	print "</body></html>" ;
}

function print_article_search ( $language , $project , $ns = 0 ) {
	global $q , $mode ;
	$url = "http://$language.$project.org/w/api.php?action=opensearch&search=".urlencode($q)."&format=xml&namespace=$ns" ;
	if ( $ns == 6 ) $url .= '&limit=64' ;
	$data = file_get_contents ( $url ) ;
	$data = new SimpleXMLElement($data) ;
	$data = $data->Section[0] ;
	$style = 'padding-top:2px;padding-bottom:2px' ;
	foreach ( $data->Item AS $d ) {
		if ( strtolower ( substr ( $d->Text[0] , -4 ) ) == '.ogg' and $ns == 6 ) continue ; // Don't show videos in image mode!
		print "<tr style='margin-bottom:10px'>" ;
		
		if ( isset ( $d->Image ) ) print "<td style='$style'>" ;
		else print "<td colspan='2' style='$style'>" ;
		
		
		print "<div style='margin:2px'><a href='" . $d->Url . "'><b>" . $d->Text . "</b></a><br/>" ;
		print $d->Description . "</div></td>" ;

		if ( isset ( $d->Image ) ) {
			$a = $d->Image ;
			$src = $a['source'] ;
			$w = $a['width'] ;
			$h = $a['height'] ;
			print "<td style='$style'>" ;
			print "<a href='" . $d->Url . "'>" ;
			print "<img border=0 src='$src' width='$w' height='$h' /></a></td>" ;
		}

		print "</tr>" ;
		$style = 'padding-top:2px;padding-bottom:2px;border-top:1px solid #DDDDDD' ;
	}
//			print "<pre>" ; print_r ( $d->Image ) ; print "</pre>" ;
}

function print_image_search ( $language , $project ) {
	global $q , $mode ;
	$ns = 6 ;
	$url = "http://$language.$project.org/w/api.php?action=opensearch&search=".urlencode($q)."&format=xml&namespace=$ns" ;
	if ( $ns == 6 ) $url .= '&limit=64' ;
	$data = file_get_contents ( $url ) ;
	$data = new SimpleXMLElement($data) ;
	$data = $data->Section[0] ;
	$style = 'padding:5px;text-align:center;vertical-align:middle' ;
	$cnt = 0 ;
	foreach ( $data->Item AS $d ) {
		if ( strtolower ( substr ( $d->Text[0] , -4 ) ) == '.ogg' and $ns == 6 ) continue ; // Don't show videos in image mode!
		
		if ( $cnt % 8 == 0 ) print "<tr>" ;

		$a = $d->Image ;
		$src = $a['source'] ;
		$w = $a['width'] ;
		$h = $a['height'] ;
		$w = floor ( $w * 1.5 ) ;
		$h = floor ( $h * 1.5 ) ;
		print "<td style='$style'>" ;
		print "<a href='" . $d->Url . "'>" ;
		print "<img src='$src' width='$w' height='$h' border='0' title='" . htmlspecialchars($d->Text) . "' /></a></td>" ;
		

		$cnt++ ;
		if ( $cnt % 8 == 0 ) print "</tr>" ;
	}
	if ( $cnt % 8 != 0 ) print "</tr>" ;
//			print "<pre>" ; print_r ( $d->Image ) ; print "</pre>" ;
}

function print_av_search ( $language , $project , $ns = 0 ) {
	global $q , $mode ;
	$tw = 120 ; // Thumbnail max width
	$av = $mode == 'av' ;
	if ( $av ) $q .= ' ogg' ;
	$url = "http://$language.$project.org/w/api.php?action=query&list=search&srsearch=" . urlencode ( $q ) . "&srnamespace=$ns&srwhat=text&format=xml" ;
	$data = file_get_contents ( $url ) ;
	$data = new SimpleXMLElement($data) ;
	$data = $data->query->search ;
	foreach ( $data->p AS $d ) {
		if ( strtolower ( substr ( $d['title'] , -4 ) ) != '.ogg' ) continue ; // Don't show images in AV mode!
		
		$fn = substr ( $d['title'] , 5 ) ; // Removing "File:"
		$n = db_get_image_data ( $fn , $language , $project ) ;
		
		$desc = $n->img_description ;
		$thumb = 'http://commons.wikimedia.org/skins-1.5/common/images/icons/fileicon-ogg.png' ;
		$w = '' ;
		if ( $n->img_media_type == 'VIDEO' )  {
			$thumb = get_thumbnail_url ( $language , $fn , $tw , $project ) ;
			if ( $n->img_width > $n->img_height ) {
				if ( $n->img_width > $tw ) $w = "width='".$tw."px'" ;
			} else {
				if ( $n->img_height > $tw ) $w = "height='".$tw."px'" ;
			}
		}
		
		
		print "<tr style='margin-bottom:10px;max-width:400px'>" ;
		print "<td style='$style'>" ;
		// get_thumbnail_url ( $lang , $image , $width , $project = "wikipedia" ) {
		print "<b><a href='" . get_wikipedia_url ( $language , $d['title'] , "" , $project )  . "'>" . $d['title'] . "</a></b> [" . $n->img_media_type . "]<br/>" ;
		print htmlspecialchars ( $desc ) ;
		print "</td><td style='$style'>" ;
		print "<a href='" . get_image_url ( $language , $fn , $language == 'commons' ? 'wikipedia' : $project ) . "'>" ;
		print "<img border=0 src='$thumb' $w /></a>" ;
//		print "<pre>" ; print_r ( $n ) ; print "</pre>" ; 
		print "</td></tr>" ;
		$style = 'padding-top:2px;padding-bottom:2px;border-top:1px solid #DDDDDD' ;
	}
//	print "<tr><td><pre>" ; print_r ( $data ) ; print "</pre></td></tr>" ;
}

function print_map_search () {
	global $q ;
	$url = "http://ws.geonames.org/search?style=full&name_equals=" . urlencode ( $q ) ;
	$data = file_get_contents ( $url ) ;
	$data = new SimpleXMLElement($data) ;
	$out = array () ;
	foreach ( $data->geoname AS $g ) {
		$lat = $g->lat ;
		$lng = $g->lng ;
		
		$url = "http://www.openstreetmap.org/index.html?mlat=$lat&mlon=$lng&zoom=12" ;
		$ak1 = '' ;
		if ( isset ( $g->adminName1 ) ) $ak1 = ', ' . $g->adminName1 ;
		else if ( isset ( $g->adminCode1 ) ) $ak1 = ', ' . $g->adminCode1 ;
		if ( $ak1 == ', ' ) $ak1 = '' ;
	
		$out[(string)$g->countryName][] = "<a href='$url'>" . $g->name . $ak1 . "</a>" ;
	
//		print "<tr>" ;
//		print "<td><a href='$url'>" . $g->name . "</a> (" . $g->countryName . "$ak1)</td>" ;
//		print "</tr>" ;
	}
	
	ksort ( $out ) ;
	
	foreach ( $out AS $c => $d ) {
		print "<tr><td><b>$c</b><ol>" ;
		foreach ( $d AS $g ) print "<li>$g</li>" ;
		print "</ol></td></tr>" ;
	}
	
//	print "<tr><td>$url<pre>" ; print_r ( $data ) ; print "</pre></td></tr>" ;
}

function show_search_results() {
	global $q , $mode ;
	print "<table style='border:2px solid #DDDDDD;padding:2px;margin-left:25%;margin-right:25%' cellpadding=0 cellspacing=0>" ;
	if ( $mode == 'image' ) {
		print_image_search ( 'commons' , 'wikimedia' , 6 ) ;
	} else if ( $mode == 'av' ) {
		print_av_search ( 'commons' , 'wikimedia' , 6 ) ;
	} else if ( $mode == 'news' ) {
		print_article_search ( 'en' , 'wikinews' ) ;
	} else if ( $mode == 'dict' ) {
		print_article_search ( 'en' , 'wiktionary' ) ;
	} else if ( $mode == 'map' ) {
		print_map_search () ;
	} else { # DEFAULT : 'pedia'
		print_article_search ( 'en' , 'wikipedia' ) ;
	}
	print "</table>" ;
}



show_entry_form() ;

?>