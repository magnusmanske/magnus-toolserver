<?PHP

include "php/common.php" ;
include "php/legacy.php" ;

$test = get_request ( 'test' , false ) ;

print '<html><head><meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" /></head><body' ;
if ( isset ( $_REQUEST['doit'] ) ) {
	print " onload=\"document.getElementsByTagName('form')[0].submit()\"" ;
}
print '>' ;

print get_common_header ( "bibber.php" , "Bibber" ) ;
print "<h1>Bibber</h1>" ;
print "<p>Schlägt Ersetzungen von Referenzen durch {{BibISBN}} vor.</p>" ;


$title = get_request ( 'title' , '' ) ;

if ( $title == '' ) {
	print "<form method='get' action='./bibber.php'>Artikel : " ;
	print "<input type='text' name='title' value='' />" ;
	print "<input type='submit' name='submit_title' value='Änderungen vorschlagen' /></form>" ;
	exit ;
}

$wiki = file_get_contents ( "http://de.wikipedia.org/w/index.php?action=raw&title=" . myurlencode ( $title ) ) ;

// Peform replacement
if ( isset ( $_REQUEST['doit'] ) ) {
	$cnt = get_request ( 'count' , 0 ) ;
	for ( $c = 1 ; $c <= $cnt ; $c++ ) {
		if ( !isset ( $_REQUEST["replace$c"] ) ) continue ;
		$orig = urldecode ( get_request ( "orig$c" ) ) ;
		$new = urldecode ( get_request ( "new$c" ) ) ;
//		print htmlspecialchars ( $orig ) . "<hr/>" ;
//		print htmlspecialchars ( $new ) . "<hr/>" ;
		$wiki = str_replace ( $orig , $new , $wiki ) ;
	}
	$button = cGetEditButton ( htmlspecialchars ( $wiki ) , $title , 'de' , 'wikipedia' , 'Ersetzen von Referenztext durch BibISBN' , 'Änderungen durchführen' , false , false , true , true , -1 , true ) ;
	print $button ;
	print " (wird durch JavaScript automatisch geklickt)" ;
//	print "<pre>" . htmlspecialchars ( $wiki ) . "</pre>" ;
	print "</body></html>" ;
	exit ;
}

function render_html ( $wiki ) {
	global $title ;
	$url = "http://de.wikipedia.org/w/api.php?format=php&action=parse&title=" ;
	$url .= myurlencode ( $title ) . "&text=" . urlencode ( $wiki ) ;
	$r = unserialize ( file_get_contents ( $url ) ) ;
	$r = $r['parse']['text']['*'] ;
	return "<span style='color:#4985D6'>$r</span>" ;
}

// Parse wikitext
$cnt = 0 ;

function print_item ( $r1 , $text , $r2 ) {
	global $cnt ;
	$isbn_pattern = "/ISBN\s*[=]{0,1}\s*([0-9- ]+[xX]{0,1})/" ;
	$isbn = array () ;
	if ( 1 != preg_match ( $isbn_pattern , $text , $isbn ) ) return ;
	$isbn = array_shift ( $isbn ) ;
	$id = preg_replace ( "/[^\dxX]/" , '' , $isbn ) ;
	$id = strtoupper ( $id ) ;

	$wq = new WikiQuery ( 'de' , 'wikipedia' ) ;
	$existing = $wq->get_existing_pages ( array ( "Vorlage:BibISBN/$id" ) ) ;
	$existing = count ( $existing ) ;

	$page = '' ;
	$pages = array () ;
	if ( preg_match ( '/S.\s*(\d+\s*-\s*\d+)\b/' , $text , $pages ) ) {
		$page = '|Seite=' . $pages[1] ;
	} else if ( preg_match ( '/S.\s*(\d+\s*f{0,2})\b/' , $text , $pages ) ) {
		$page = '|Seite=' . trim ( $pages[1] ) ;
	} else if ( preg_match ( '/Seite\s+(\d+\s*f{0,2})\b/' , $text , $pages ) ) {
		$page = '|Seite=' . trim ( $pages[1] ) ;
	}

	$newwiki_core = "{{BibISBN|$id$page}}" ;
	$newwiki = "$r1$newwiki_core$r2" ;
	
	$makenew = '' ;
	if ( $existing == 0 ) {
		$url = "http://toolserver.org/~magnus/bibcomm.php?mode=new&type=isbn&entry_isbn=$id" ; //&prefill=google" ;
		$url .= '&dummytext=' . urlencode ( $text ) ;
		$makenew = "Neue BibISBN-Vorlage erstellen [<a target='_blank' href='$url'>Aus Text</a>] / " ;
		$makenew .= " [<a target='_blank' href='$url&prefill=google'>Aus Text und Google prefill</a>]" ;
	}

	$cnt++ ;
	$col = $existing ? 'green' : 'red' ;
	$checked = $existing ? 'checked' : '' ;
	$rowspan = $existing ? 'rowspan=2' : '' ;
	print "<tr>" ;
	print "<td><span style='color:$col'>" ;
	print htmlspecialchars ( $r1 ) . "<br/>" ;
	print htmlspecialchars ( $text ) . "<br/>" ;
	print htmlspecialchars ( $r2 ) . "</span>$makenew" ;
	print "<input type='hidden' name='orig$cnt' value='" . urlencode($r1.$text.$r2) . "' />" ; ;
	print "</td>" ;
	print "<td><textarea rows=3 cols=80 name='new$cnt'>$newwiki</textarea>" ;
	print "</td>" ;
	print "<td $rowspan><input type='checkbox' name='replace$cnt' value='1' $checked/></td>" ;
	print "</tr>" ;


	if ( $existing ) {
		print "<tr><td valign='top'>" . render_html ( trim ( $text ) ) ;
		print "</td><td valign='top'>" . render_html ( $newwiki_core ) ;
		print "<a target='_blank' href='http://de.wikipedia.org/w/index.php?action=edit&title=Vorlage:BibISBN/$id'>Vorlage bearbeiten</a></td></tr>" ;
	}

//	print htmlspecialchars ( "$r1|$text|$r2" ) . " : $isbn/$id<br/>" ;
}

print "<form method='post' action='./bibber.php'>" ;
print "<table border=1><tr><th>Referenz</th><th>Neuer Text</th><th>Ersetzen</th></tr>" ;

// Referenzen
$a = explode ( '<ref' , " $wiki" ) ;
array_shift ( $a ) ;
foreach ( $a AS $r ) {
	if ( substr ( $r , 0 , 1 ) != ' ' && substr ( $r , 0 , 1 ) != '>' ) continue ;
	$r = explode ( '>' , $r , 2 ) ;
	if ( count ( $r ) != 2 ) continue ;
	$r1 = array_shift ( $r ) ;
	$r1t = trim ( $r1 ) ;
	if ( substr ( $r1t , -1 , 1 ) == '/' ) {
		continue ;
	}
	$r1 = "<ref$r1>" ;
	$r = array_shift ( $r ) ;
	$r = explode ( '</ref>' , $r , 2 ) ;
	if ( count ( $r ) != 2 ) continue ;
	$text = array_shift ( $r ) ;
	$r2 = "</ref>" ;
	
	$all = "$r1$text$r2" ;
//	if ( $test ) print htmlspecialchars ( $all ) . "!<br/>" ;
	if ( 0 == substr_count ( $wiki , $all ) ) continue ;

	print_item ( $r1 , $text , $r2 ) ;

}


// Literatur
$lines = explode ( "\n" , $wiki ) ;
foreach ( $lines AS $l ) {
	if ( substr ( $l , 0 , 1 ) != '*' ) continue ;
	if ( preg_match ( '/\<ref/' , $l ) ) continue ;
	$r1 = '' ;
	while ( substr ( $l , 0 , 1 ) == '*' ) {
		$l = substr ( $l , 1 ) ;
		$r1 .= '*' ;
	}
//	$l = trim ( $l ) ;
	print_item ( $r1 , $l , "" ) ;
}

print "</table><input type='hidden' name='count' value='$cnt' />" ;
print "<input type='hidden' name='title' value='$title' />" ;
print "<input type='submit' name='doit' value='Ersetzungen durchführen' /> (finale Kontrolle in Wikipedia-Bearbeitungs-Diff)</form>" ;

print "
<h3>Hinweise</h3>
<p>Referenzen für vorhandene BibISBN-Vorlagen <span style='color:green'>in grün</span>, nicht vorhandene <span style='color:red'>in rot</span>.</p>
Ersetzungs-Beispiele :<br/>
<pre>
{{BibISBN|IDENTIFIER}}
{{BibISBN|IDENTIFIER|Seite=VON-BIS}}
{{BibISBN|IDENTIFIER|Kommentar}}
{{BibISBN|IDENTIFIER|Kommentar|Seite=VON-BIS}}
</pre>
" ;


//print "<pre>" . htmlspecialchars($wiki) . "</pre>" ;

print "</body></html>" ;

?>