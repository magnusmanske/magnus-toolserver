﻿<?PHP
error_reporting ( E_ALL ) ;
$suppress_gz_handler = 1 ;
@set_time_limit ( 15*60 ) ; # Time limit 45min

include_once ( 'queryclass.php' ) ;
high_mem ( 64 , 'image_paranoia' ) ;

function db_pages_in_templates ( $language , $project , $templates , $ns = 0 , $chop_prefix = false ) {
	$mysql_con = db_get_con_new($language,$project) ;
	$db = get_db_name ( $language , $project ) ;
	foreach ( $templates AS $k => $v ) {
		if ( $chop_prefix ) $templates[$k] = array_pop ( explode ( ':' , $templates[$k] , 2 ) ) ;
		make_db_safe ( $templates[$k] ) ;
		$templates[$k] = '"' . $templates[$k] . '"' ;
	}
	$templates = implode ( ',' , $templates ) ;
	
	$ret = array () ;
	$sql = "SELECT ".get_tool_name()." DISTINCT page_title FROM page,templatelinks WHERE page_namespace={$ns} AND page_id=tl_from AND tl_namespace=10 AND tl_title IN ( $templates )" ;
	$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
	while ( $o = mysql_fetch_object ( $res ) ) {
		$ret[] = $o->page_title ;
	}
	return $ret ;
}

function db_local_used_images_extended ( $language , $project , $all_images ) {
	$mysql_con = db_get_con_new($language,$project) ;
	$db = get_db_name ( $language , $project ) ;
	
	$ret = array () ;

	while ( count ( $all_images ) > 0 ) {
		$images = array () ;
		while ( count ( $all_images ) > 0 and count ( $images ) < 100 ) {
			$images[] = array_pop ( $all_images ) ;
		}
	
		$imagelist = '' ;
		foreach ( $images AS $i ) {
			if ( $imagelist != '' ) $imagelist .= ',' ;
			make_db_safe ( $i ) ;
			$imagelist .= '"' . $i . '"' ;
		}
		
		$sql = "SELECT ".get_tool_name()." page_title,page_namespace,il_to FROM page,imagelinks WHERE page_id=il_from AND il_to IN ( $imagelist )" ;
		$res = mysql_db_query ( $db , $sql , $mysql_con ) ;
		if ( mysql_errno() != 0 ) {
			print mysql_error() ;
			return $ret ;
		}
		
		while ( $o = mysql_fetch_object ( $res ) ) {
			$n = $o->page_namespace . ':' . $o->page_title ;
			if ( isset ( $ret[$o->il_to] ) ) $ret[$o->il_to] .= "|$n" ;
			else $ret[$o->il_to] = $n ;
		}
		mysql_free_result ( $res ) ;
	}
	return $ret ;	
}


function show_local_images ( $language , $project , &$images , $local ) {
	global $nsdata ;
	$used = db_local_used_images_extended ( $language , $project , $images ) ;
	
	print "<p>" . count ( $used ) . " of " . count ( $images ) . " suspect images are used on $language.$project.</p>" ;
	
	print "<ol>" ;
	foreach ( $used AS $image => $pages ) {
		$pages = explode ( '|' , $pages ) ;
		$pages2 = array () ;
		foreach ( $pages AS $k => $v ) {
			$v = explode ( ':' , $v , 2 ) ;
			$ns = array_shift ( $v ) ;
			$v = array_pop ( $v ) ;
			if ( $ns != 0 ) $v = $nsdata[$ns]['*'] . ":$v" ;
			$url = get_wikipedia_url ( $language , $v , '' , $project ) ;
			if ( $ns != 0 ) $v = "<i>$v</i>" ;
			$pages2[$k] = "<a target='_blank' href=\"$url\">$v</a>" ;
		}
		if ( count ( $pages ) == 1 ) {
			$page_out = array_pop ( $pages2 ) ;
		} else {
			$page_out = "<ul><li>" . implode ( '</li><li>' , $pages2 ) . "</li></ul>\n" ;
		}
		
		if ( $local ) $image_url = get_wikipedia_url ( $language , "Image:$image" , '' , $project ) ;
		else $image_url = get_wikipedia_url ( 'commons' , "Image:$image" , '' , 'wikimedia' ) ;
		
		print "<li><a target='_blank' href=\"$image_url\"><b>".$nsdata[6]['*'].":$image</b></a> : $page_out</li>\n" ;
	}
	print "</ol>" ;
	myflush() ;
}





$page = 'Wikipedia:Dateiüberprüfung/Problematische Commonsdateien' ;
$language = 'de' ;
$project = 'wikipedia' ;

$url = "http://$language.$project.org/w/api.php?action=query&meta=siteinfo&siprop=namespaces&format=php" ;
$nsdata = unserialize ( file_get_contents ( $url ) ) ;
$nsdata = $nsdata['query']['namespaces'] ;

/*
$url = "http://$language.$project.org/w/index.php?action=raw&title=" . myurlencode ( $page ) ;
$text = file_get_contents ( $url ) ;
$lines = explode ( "\n" , $text ) ;
$commons_templates = array () ;
$local_templates = array () ;
foreach ( $lines AS $line ) {
	$line = trim ( $line ) ;
	$use = false ;
	while ( preg_match ( '/^[*+:]+/' , $line ) ) {
		$use = true ;
		$line = substr ( $line , 1 ) ;
	}
	if ( !$use ) continue ;
	$line = str_replace ( '[[' , '' , $line ) ;
	$line = str_replace ( ']]' , '' , $line ) ;
	$line = trim ( $line ) ;
	if ( preg_match ( '/^commons:/i' , $line ) ) {
		$commons_templates[] = substr ( $line , 8 ) ;
	} else {
		$local_templates[] = $line ;
	}
}
*/

$commons_templates = array () ;
$commons_templates[] = 'Template:PD-US' ;
$commons_templates[] = 'Template:PD-US-not renewed' ;
$commons_templates[] = 'Template:PD-US-no notice' ;

$local_templates = array () ;
$local_templates[] = 'Vorlage:Geschützt' ;
$local_templates[] = 'Vorlage:Geschützt-Ungeklärt' ;

$mode = '12' ;
if ( isset ( $_REQUEST['mode'] ) )  $mode = $_REQUEST['mode'] ;


print "<html>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print "<body>" ;
print get_common_header ( "image_paranoia.php" ) ;


if ( false !== strpos ( $mode , '1' ) ) {
	print "<h3>Local templates</h3>" ;
	print "<p>(" . implode ( ', ' , $local_templates ) . ")</p>" ; myflush() ;
	$images = db_pages_in_templates ( $language , $project , $local_templates , 6 , true ) ;
	show_local_images ( $language , $project , $images , true ) ;
	print "\n" ; myflush() ;
}

if ( false !== strpos ( $mode , '2' ) ) {
	print "<h3>Commons templates</h3>" ;
	print "<p>(" . implode ( ', ' , $commons_templates ) . ")</p>" ; myflush() ;
	$images = db_pages_in_templates ( 'commons' , 'wikimedia' , $commons_templates , 6 , true ) ;
	show_local_images ( $language , $project , $images , false ) ;
	print "\n" ; myflush() ;
}

print "</body>" ;
print "</html>\n" ;
myflush() ;



?>